#!/usr/bin/env node
const program = require('commander');
const fs = require('fs');
const path = require('path');
const archiver = require('archiver');

program
    .arguments('<module>')
    .arguments('<folder>')
    .action((mod, dir) => {
        const PREFIX = mod;

        const ignore = [
            ".git",
            ".idea",
            ".gitlab-ci.yml",
            ".gitignore",
            `${PREFIX}.zip`,
        ];
                
        const files = fs.readdirSync(dir).filter(f => !ignore.includes(f));
        
        const archive = archiver('zip');
        const output = fs.createWriteStream(`${PREFIX}.zip`);
    
        archive.pipe(output);
        
        files.forEach( f => {
            const fpath = path.join(dir, f);
            const finfo = fs.lstatSync(fpath);
            
            if(finfo.isDirectory()) {
                archive.directory(fpath, `${PREFIX}/${f}`);
            } else {
                archive.file(fpath, {name: `${PREFIX}/${f}`});
            }
        });
        

        archive.finalize();

    })
    .parse(process.argv);

